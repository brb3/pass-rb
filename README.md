# pass-rb

Rewrite of `pass` in Ruby. `pass` was written by Jason A. Donenfeld and is
available at [passwordstore.org](https://www.passwordstore.org/). This project
aims to be a drop-in replacement for Jason's utility, and borrows heavily from
its interface.

## But Why?

Because I just really don't like writing code with `bash`. That's the only
reason I've made this.

Hopefully, rewriting in Ruby will make it easier to hack on and add
functionality.

## Contributing

Just fork, write some code, and make a Merge Request. Ideally, I'd like for the
core features to stay very similar to passwordstore's functionality to maintain
compatibility. But it's safe to venture off and make changes where it makes
sense from a UI perspective, and where it will not break _too_ much.
